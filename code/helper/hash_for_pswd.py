import hashlib
import uuid


def password_hash(password):
    """Функция ожидает на вход строку, после чего хеширует ее,
    шифруя алгоритмом SHA256, добвляя случайно сгенерированные
    занчения
    """
    salt = uuid.uuid4().hex
    return hashlib.sha256(
        salt.encode() + password.encode()
    ).hexdigest() + ':' + salt
