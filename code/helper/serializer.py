class AuthDataSerializer:
    """Класс который ожидает на вход две строки
    и используется для фактического их хранения.

    """

    def __init__(self, user_name: str, password: str):
        self.user_name = user_name
        self.password = password


def serialize(serializer_data) -> dict:
    """Функция которая ожидает на вход результат работы
    класса AuthDataSerializer и возвращает словарь полученный
    из полей вышеуказанного класса.

    :param serializer_data:
    :return: dict

    """
    return {
        'user_name': serializer_data.user_name,
        'password': serializer_data.password,
    }


def deserialize(deserializer_data):
    """Функция которая ожидает на вход словарь и возвращает
    работу класса AuthDataSerializer который получает аргументами
    значения ключей словаря.

    :param deserializer_data:
    :return:

    """
    return AuthDataSerializer(
        user_name=deserializer_data['user_name'],
        password=deserializer_data['password'],
    )
