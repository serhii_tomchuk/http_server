from dbdata.db import DbConnect

"""Место из которого можно передавать работу разных классов
инициализируя их здесь, в данном случае инициализирован класс
DbConnect который создает БД

"""
db = DbConnect()
conn = db.connections()
db.create_db(conn=conn)
