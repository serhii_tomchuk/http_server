from constants_data import SERVER_DATA
from web_server.web_server import start_web_server

"""Здесь запускается WebServer"""
if __name__ == '__main__':
    start_web_server(server_data=SERVER_DATA)
