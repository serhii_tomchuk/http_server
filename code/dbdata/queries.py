"""Запросы в базу данных"""

GET_ALL_DATA = 'SELECT * FROM USERS'

GET_ALL_ID = 'SELECT id FROM USERS'

GET_DATA_ID = '''
            SELECT id, user_name, password
            FROM `USERS`
            WHERE `id`=?'''

CREATE_DB = '''
            CREATE TABLE IF NOT EXISTS USERS (
                    id          INTEGER PRIMARY KEY NOT NULL,
                    user_name     TEXT,
                    password    TEXT,
                    UNIQUE(user_name)
                )'''

INSERT_DB = '''
                    INSERT OR IGNORE INTO USERS (
                    user_name,
                    password
                    ) VALUES (?, ?)'''
