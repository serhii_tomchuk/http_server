import sqlite3

from dbdata.queries import (
    CREATE_DB,
    INSERT_DB
)


class DbConnect:
    """Класс который отвечает за работу с БД.

    Methods
    -------
        connections()
            Метод который подключается к БД.

        create_db(conn)
            Метод который ожидает на вход подключение к БД и
            создает в базе таблицу, с помощью переданного query CREATE_DB,
            в том случае если она не была создана до этого.


        insert_data(self, conn, user_name: str, password: str)
            Метод который ожидает на вход подключение и данные, создает новую
            строку в таблице БД, с помощью query INSERT_DB, в том случае если
            передаваемое значение для столбца user_name является UNIQUE.
    """

    def connections(self):
        """Метод который подключается к БД.

        :return:
        """
        with sqlite3.connect('dbdata/SQLite3.db') as conn:
            return conn

    def create_db(self, conn):
        """Метод который ожидает на вход подключение к БД и
        создает в базе таблицу переданную в query CREATE_DB,
        в том случае если она не было создана до этого.

        :param conn:
        :return:
        """
        c = conn.cursor()
        c.execute(CREATE_DB)
        conn.commit()

    def insert_data(self, conn, user_name: str, password: str):
        """Метод который ожидает на вход подключение и данные, создает новую
        строку в таблице БД, с помощью query INSERT_DB, в том случае если
        передаваемое значение для столбца user_name является UNIQUE.

        :param conn:
        :param user_name: str
        :param password: str
        :return:
        """
        c = conn.cursor()
        c.execute(INSERT_DB, (user_name, password))
        conn.commit()
