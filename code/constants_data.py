"""Хранилище в виде словаря для констант которые
используются в разных местах программы
"""
SERVER_DATA = {
    'server': {
        'HOST': [
            '',
            '127.0.0.1',
            'http://localhost'
        ],
        'PORT': 8000
    },
    'GET/POST': {
        'BASE_PATH': '/',
        'API': '/api',
        'DETAIL': '/detail?id=',
        'content-types': {
            'html': 'text/html',
            'json': 'application/json',
        }
    }
}
