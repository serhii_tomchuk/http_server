from constants_data import SERVER_DATA

"""Хранилище в виде списка для ссылок отображаемых
на главной странице.
"""

urls = [
    SERVER_DATA["GET/POST"]["API"],
    # SERVER_DATA["GET/POST"]["API"] +
    # SERVER_DATA["GET/POST"]["DETAIL"] + f'{1}',
    'something1',
    'something2',
    '.....'
]
