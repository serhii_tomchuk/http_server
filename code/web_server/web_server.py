import cgi
import json
from http.server import (
    BaseHTTPRequestHandler,
    HTTPServer
)
from typing import List, Tuple

import simplejson

from constants_data import SERVER_DATA
from dbdata.queries import (
    GET_ALL_DATA,
    GET_ALL_ID,
    GET_DATA_ID
)
from helper.hash_for_pswd import password_hash
from helper.loader import db
from helper.serializer import deserialize
from .pages import urls


class WebServer(BaseHTTPRequestHandler):
    """Данный класс предоставляет возможность обрабатывать
    GET и POST запросы.

    Methods
    -------
        do_GET()
            Метод выполняет прверку, если path == '/',
            вызывается приватные методы _html и _form которые
            рендерят html страницу со списком доступных urls,
            так же полученные из БД ides используются в рендеринге
            кнопок для перехода к деталям данных,
            формой для отправки данных на сервер;
            если path == '/api', вызывается приватный метод
            _json который получает из БД все данные в переменную all_data
            с помощью запроса GET_ALL_DATA и рендерит их на странице в
            формате json;
            если path == '/api/detail?api=<id from db>', дает возможность
            данные по конкретному id.

            :exception
                В случае не установленного соединения с БД, будет
                вызвано исключение.

        do_POST()
            Метод выполняет проверку, если path == '/',
            он получает данные отправленные через форму на странице,
            вызывает функцию deserialize десиарелизирует данные преобразуя
            из в строку, вызывает метод insert_data класса DbConnect и кладет
            данные в БД;
            если path == '/api' перехватывает данные request.post(), вызывает
            функцию deserialize десиарелизирует данные из в строку, вызывает
            метод insert_data класса DbConnect и кладет данные в БД

        _headers(content_type: str)
            Приватный метод принимает на вход content_type и в зависимости от
            переданного значения формирует HTTP Headers

        _html(html: str, urls: List[str])
            Приватный метод который на вход принимает пустую строку html и
            список ссылок для отображения на странице urls, вызывает приватный
            метод _headers передает ему content_type "text/html", получает
            нужный HTTP Headers и рендерит список ссылок.

        _form(hml: str)
            Приватный метод который ожидает на вход пустую строку и рендерит
            форму для отправки данных на сервер через страницу html POST
            запросом.

        _detail_buttons(html)
            Приватный метод который на вход ожидает строку и рендерит таблицу
            кнопок с id строк из БД.

        _json(all_data: List[Tuple[str]]) -> dict
            Приватный метод который ожидает на вход массив данных из БД, если
            данные преданы генерирует список словарей data который передается
            параметром в возвращаемый словарь для дальнейших зпросов, если
            массив не передан data = [None].
    """

    def _headers(self, content_type: str):
        """ Приватный метод принимает на вход content_type и в зависимости от
        переданного значения формирует HTTP Headers

        :param content_type: str
        :return:
        """
        self.send_response(200)
        self.send_header(
            'content-type',
            SERVER_DATA["GET/POST"]["content-types"][f"{content_type}"]
        )
        self.end_headers()

    def do_GET(self):
        """Метод выполняет прверку, если path == '/',
            вызывается приватные методы _html и _form которые
            рендерят html страницу со списком доступных urls,
            так же полученные из БД ides используются в рендеринге
            кнопок для перехода к деталям данных,
            формой для отправки данных на сервер;
            если path == '/api', вызывается приватный метод
            _json который получает из БД все данные в переменную all_data
            с помощью запроса GET_ALL_DATA и рендерит их на странице в
            формате json;
            если path == '/api/detail?api=<id from your db>', дает возможность
            данные по конкретному id.

        :exception
            В случае не установленного соединения с БД, будет
            вызвано исключение.

        :return:
        """
        html = ''
        try:
            if self.path.endswith(
                    SERVER_DATA["GET/POST"]["BASE_PATH"]
            ):
                self._html(
                    html=html,
                    urls=urls
                )
                self._detail_buttons(html=html)
                self._form(html=html)
            if self.path.endswith(
                    SERVER_DATA["GET/POST"]["API"]
            ):
                self.wfile.write(
                    json.dumps(
                        self._json(
                            all_data=db.connections().execute(
                                GET_ALL_DATA
                            ).fetchall()
                        )
                    ).encode(encoding='utf_8')
                )
            # everything below in this method is not described in the docstring
            for id_ in db.connections().execute(
                    GET_ALL_ID
            ).fetchall():
                if self.path.endswith(
                        SERVER_DATA["GET/POST"]["API"]
                        + SERVER_DATA["GET/POST"]["DETAIL"]
                        + f'{id_[0]}'
                ):
                    self.wfile.write(
                        json.dumps(
                            self._json(
                                all_data=db.connections().execute(
                                    GET_DATA_ID,
                                    (self.path.split('=')[-1],)
                                ).fetchall()
                            )
                        ).encode(encoding='utf_8')
                    )
        except Exception as err:
            print(err)

    def _form(self, html: str):
        """Приватный метод который ожидает на вход пустую строку и рендерит форму для
        отправки данных на сервер через страницу html POST запросом.

        :param html: str
        :return:
        """

        html += '<html><body>'
        html += '<h1><b>Add your auth data</b></h1>'
        html += '<form method="POST" enctype="multipart/form-data" action="/">'
        html += '<input name="user_name" type="text" placeholder="Add your username">'  # noqa
        html += '<input name="password" type="text" placeholder="Add your password">'  # noqa
        html += '''
        <button type="submit">Add</button>
        '''
        html += '</form>'
        html += '<hr>'
        html += '<h4><b>Author: Serhii Tomchuk</b></h4>'
        html += '<hr>'
        html += 'phone: +38093548372'
        html += '<hr>'
        html += 'Linked: <a href=' \
                'https://www.linkedin.com/in/serhii-tomchuk-62a72b1a0/>' \
                'Serhii Tomchuk</a>'
        html += '<hr>'
        html += '</html></body>'
        self.wfile.write(html.encode())

    def _html(self, html: str, urls: List[str]):
        """Приватный метод который на вход принимает пустую строку
        html и список ссылок для отображения на странице urls, вызывает
        приватный метод _headers передает ему content_type "text/html",
        получает нужный HTTP Headers и рендерит список ссылок.

        :param html: str, urls: List[str]
        :return:
        """
        self._headers(content_type="html")
        html += '<html><body>'
        html += '<h1>Links list</h1>'
        for url in urls:
            html += f'<h4><a href="{url}">Push it - {url}</a></h4>'
            html += '</br>'

        self.wfile.write(html.encode())

    def _detail_buttons(self, html):
        """Приватный метод который на вход ожидает строку и рендерит
        таблицу кнопок с id строк из БД.

        :param html: str
        :return:
        """
        html += '<html><body>'
        html += '<table cellspacing="5" border="5" cellpadding="4" width="99">'
        html += '<caption>Table ID data </caption>'
        html += '<tr><td>'
        for id_ in db.connections().execute(
                GET_ALL_ID
        ).fetchall():
            html += '<form>'
            html += '<button>'
            html += f'''
            <a href="http://localhost:8000/api/detail?id={id_[0]}">
            Detail-{id_[0]}
            </a>'''
            html += '</button>'
            html += '</form>'
        html += '</td></tr>'
        html += '</table>'
        html += '</html></body>'
        self.wfile.write(html.encode())

    def _json(self, all_data: List[Tuple[str]]) -> dict:
        """Приватный метод который ожидает на вход массив данных из БД,
        если данные преданы генерирует список словарей data который
        передается параметром в возвращаемый словарь для дальнейших зпросов,
        если массив не передан data = [None].

        :param all_data: List[Tuple[str]]
        :return: dict
        """
        self._headers(content_type="json")
        try:
            if all_data:
                data = [

                    {
                        "id": all_data[data_][0],
                        "user_name": f"{all_data[data_][1]}",
                        "password": f"{all_data[data_][2]}"
                    }

                    for data_ in range(len(all_data))
                ]
            else:
                data = [f"Here for now {None}"]
            return {
                "data": data,
                "response": {
                    "status": 200,
                    "message": "ok"
                }
            }
        except Exception as err:
            print(err)

    def do_POST(self):
        """Метод выполняет проверку, если path == '/',
        он получает данные отправленные через форму на странице,
        вызывает функцию deserialize десиарелизирует данные преобразуя
        из в строку, вызывает метод insert_data класса DbConnect и кладет
        данные в БД;
        если path == '/api' перехватывает данные request.post(), вызывает
        функцию deserialize десиарелизирует данные из в строку, вызывает
        метод insert_data класса DbConnect и кладет данные в БД

        Note:
        ----
            В дальнейшем можно добавить валищдацию данных и увеличивать
            количество принимаемых данных.
        """
        # https://localhost:8000/
        if self.path.endswith(
                SERVER_DATA["GET/POST"]["BASE_PATH"]
        ):
            ctype, pdict = cgi.parse_header(self.headers.get('content-type'))
            pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
            len_count = int(self.headers.get('Content-length'))
            pdict['CONTENT-LENGTH'] = len_count
            if ctype == 'multipart/form-data':
                fields = cgi.parse_multipart(self.rfile, pdict)
                des = deserialize(
                    deserializer_data=fields
                )
                db.insert_data(
                    conn=db.connections(),
                    user_name=''.join(des.user_name),
                    password=password_hash(
                        password=''.join(des.password)
                    )
                )
                self.send_response(200)
                self.send_header('content-type', 'txt/html')
                self.send_header('Location', '/')
                self.end_headers()

        # https://localhost:8000/api
        if self.path.endswith(
                SERVER_DATA["GET/POST"]["API"]
        ):
            self._headers(content_type='json')
            self.data_string = self.rfile.read(
                int(self.headers['Content-Length'])
            )
            self.send_response(200)
            self.end_headers()
            data = simplejson.loads(self.data_string)
            print(data)
            if data["data"]:
                des = deserialize(
                    deserializer_data=data["data"][0]
                )
                db.insert_data(
                    conn=db.connections(),
                    user_name=des.user_name,
                    password=password_hash(
                        password=des.password
                    )
                )


def start_web_server(server_data: dict):
    """Функция которая ожидает словарь констант server_data и запускает
    сервер.

    :return:
    """
    _server_address = (
        server_data["server"]["HOST"][0],
        server_data["server"]["PORT"]
    )
    server = HTTPServer(
        _server_address,
        WebServer
    )
    print(
        f'Server running on address {server.server_address}\n'
        f'http://localhost:8000'
    )
    server.serve_forever()
