import json

import requests

from constants_data import SERVER_DATA
from helper.serializer import AuthDataSerializer, serialize

serializer_data = AuthDataSerializer(
    user_name='NewUser',  # Введите username (UNIQUE field in DB)
    password='zdq'  # Введите пароль
)
"""Инициализация класса AuthDataSerializer передавая данные для сиреализации"""

data_id = 15
"""Инициализация переменной для передачи в ней id (requests)"""


def get_api(server_data):
    """Функция которая ожидает на вход словарь констант и получет все данные
    в формте json

    :param: server_data: dict

    :return:
    """
    res = requests.get(
        f'''
        {server_data["server"]["HOST"][2]}:
        {server_data["server"]["PORT"]}
        {server_data["GET/POST"]["API"]}
        '''.replace('\n', '').replace(' ', '')
    )
    print(res.status_code)
    print(res.json())
    return res.json()["data"][-1]["id"]


def get_detail(server_data: dict, data_id: int):
    res = requests.get(
        f'''
                {server_data["server"]["HOST"][2]}:
                {server_data["server"]["PORT"]}
                {server_data["GET/POST"]["API"]}
                {server_data["GET/POST"]["DETAIL"]}
                {data_id}
                '''.replace('\n', '').replace(' ', '')
    )
    print(res.status_code)
    print(res.json())
    return res.json()


def post_api(server_data: dict):
    """Функция которая ожидает на вход словарь констант, потом формирует payload
    для POST запроса, передавая туда сериализованные данные

    :param: server_data: dict

    :return:
    """

    try:
        payload = {
            "data": [
                serialize(
                    serializer_data=serializer_data
                )
            ]
        }
        headers = {'content-type': 'application/json'}
        res = requests.post(
            f'''
                {server_data["server"]["HOST"][2]}:
                {server_data["server"]["PORT"]}
                {server_data["GET/POST"]["API"]}
                '''.replace('\n', '').replace(' ', ''),
            data=json.dumps(payload),
            headers=headers
        )
        print(res.status_code)
        print(res.reason)
    except requests.RequestException as err:
        print(err)


if __name__ == '__main__':
    post_api(server_data=SERVER_DATA)
    data_last_id = get_api(server_data=SERVER_DATA)
    get_detail(
        server_data=SERVER_DATA,
        data_id=data_last_id
    )
