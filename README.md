\m/_ _\m/


# get request all_data
`curl -v http://localhost:8000/api`

# get request id_data 
`curl -v http://localhost:8000/api/detail?id=<--your_id_from_DB-->`

# post request
`curl -g -X POST -H Content-type:application/json --data-binary "{\"data\": [{\"user_name\": \"FirstMan-101\", \"password\": \"First||Pswd\"}]}" http://localhost:8000/api`
